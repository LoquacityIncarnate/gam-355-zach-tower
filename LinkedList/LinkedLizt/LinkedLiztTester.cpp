#include <iostream>
#include "LinkedLizt.h"
#include <string>
using namespace std;

int main() {
    LinkedLizt listA;
    listA.insert(1, 0);
    cout << "";
    cout<<("Size: " + to_string(listA.size())+"\n"); 
    for (int i = 0; i < listA.size(); i++) { cout << (to_string(i) + ": " + to_string(listA.operate(i)->data) + "\n"); }
    listA.insert(2, 1);
    cout << ("Size: " + to_string(listA.size()) + "\n");
    for (int i = 0; i < listA.size(); i++) { cout << (to_string(i) + ": " + to_string(listA.operate(i)->data) + "\n"); }
    listA.insert(5, 0);
    cout << ("Size: " + to_string(listA.size()) + "\n");
    for (int i = 0; i < listA.size(); i++) { cout << (to_string(i) + ": " + to_string(listA.operate(i)->data) + "\n"); }
    listA.erase(0);
    cout << ("Size: " + to_string(listA.size()) + "\n");
    for (int i = 0; i < listA.size(); i++) { cout << (to_string(i) + ": " + to_string(listA.operate(i)->data) + "\n"); }
    listA.clear();
    listA.insert(15, 0);
    cout << ("Size: " + to_string(listA.size()) + "\n");
    for (int i = 0; i < listA.size(); i++) { cout << (to_string(i) + ": " + to_string(listA.operate(i)->data) + "\n"); }
    cin.get();
}