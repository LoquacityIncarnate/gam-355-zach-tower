//A signly-linked LinkedList class by Zachary Tower, with insert, erase, operate, clear, and size functions.
#include <iostream>
using namespace std;
//The class for storing nodes of the LinkedList, storing the node's data (required for construction) and its connection (by default null)
template <typename T> class node
{
public:
    T data;
    node*<T> next;
    node(T d)
    {
        data = d; next = NULL;
    }
};
//A singly-linked LinkedList class
class LinkedLizt
{
    //head stores the first node of the LinkedList, and count stores the number of nodes.
    node<T>* head; int count = 0;
public:
    LinkedLizt() { head = NULL; }
    void insert(int, int);
    void erase(int);
    node<T>* operate(int);
    void clear();
    int size();
};

//Inserts a new node with the given value at the given index
void LinkedLizt::insert(int data, int index)
{
    if (index > count)
    {
        //Error: Insertion index greater than LinkedList length
        return;
    }
    else if (count == 0)
    {
        //If the list is empty, make this node the first
        node<T>* n = new node<T>(data);
        head = n;
    }
    else
    {
        //connect the node before the intended index to the new node, and connect the new node to the previous occupant of the intended index
        node* n = new node(data);
        node* currentNode = head;
        for (int i = 0; i < index - 1; i++)
        {
            currentNode = currentNode->next;
        }
        n->next = currentNode->next;
        currentNode->next = n;
    }
    count++;
    return;
}
//Removes the node at the given index, connecting the node preceding it to the node proceeding it
void LinkedLizt::erase(int index)
{
    if (index > count || count <= 0)
    {
        //Error: Erasure index greater than LinkedList length
        return;
    }
    else
    {
        node* currentNode = head;
        int i;
        for (i = 0; i < index - 1; i++)
        {
            currentNode = currentNode->next;
        }
        node* join1 = currentNode;
        currentNode = currentNode->next;
        if (i == count - 1)
        {
            join1->next = NULL;
        }
        else
        {
            node* join2 = currentNode->next;
            join1->next = join2;
        }
        currentNode = NULL;
    }
    count--;
    return;
}
node* LinkedLizt::operate(int index)
{
    node* n = NULL;
    if (count > 0)
    {
        n = head;
        for (int i = 0; i < index; i++)
        {
            n = n->next;
        }
    }
    return n;
}
void LinkedLizt::clear()
{
    int n = count;
    for (int i = 0; i < n; i++)
    {
        erase(0);
    }
}
int LinkedLizt::size()
{
    return count;
}

