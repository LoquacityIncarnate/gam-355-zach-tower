﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedLizt
{
    //A handmade singly-linked LinkedList class, by Zachary Tower
    public class LinkedLizt<DataType>
    {
        //The class for storing nodes of the LinkedList, storing the node's data (required for construction) and its connection (by default null)
        public class node
        {
            public DataType data;
            public node next;
            public node(DataType dat)
            {
                data = dat;
                next = null;
            }
        }
        //head stores the first node of the LinkedList, and count stores the number of nodes.
        node head;
        int count = 0;
        //Inserts a new node with the given value at the given index
        public void insert(DataType data, int index)
        {
            if (index > count)
            {
                //Error: Insertion index greater than LinkedList length
                return;
            }
            else if (count == 0)
            {
                //If the list is empty, make this node the first
                node n = new node(data);
                head = n;
            }
            else
            {
                //connect the node before the intended index to the new node, and connect the new node to the previous occupant of the intended index
                node n = new node(data);
                node currentNode = head;
                for (int i = 0; i < index-1; i++)
                {
                    currentNode = currentNode.next;
                }
                n.next = currentNode.next;
                currentNode.next = n;
            }
            count++;
            return;
        }
        //Removes the node at the given index, connecting the node preceding it to the node proceeding it
        public void erase(int index)
        {
            if (index > count || count <= 0)
            {
                //Error: Erasure index greater than LinkedList length
                return;
            }
            else
            {
                node currentNode = head;
                int i;
                for (i = 0; i < index - 1; i++)
                {
                    currentNode = currentNode.next;
                }
                node join1 = currentNode;
                currentNode = currentNode.next;
                if (i == this.count-1)
                {
                    join1.next = null;
                }
                else
                {
                    node join2 = currentNode.next;
                    join1.next = join2;
                }
                currentNode = null;
            }
            count--;
            return;
        }
        //Returns the node at the given index
        public node operate(int index)
        {
            node n = null;
            if (this.count > 0)
            {
                n = head;
                for (int i = 0; i < index; i++)
                {
                    n = n.next;
                }
            }
            return n;
        }
        //Erases all nodes in the LinkedList
        public void clear()
        {
            int n = this.count;
            for (int i = 0; i < n; i++)
            {
                this.erase(0);
            }
        }
        //Returns the number of nodes in the LinkedList
        public int size()
        {
            return count;
        }
    }
}
