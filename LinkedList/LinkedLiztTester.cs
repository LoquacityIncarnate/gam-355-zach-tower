﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using LinkedLizt;


    public class LinkedLiztTester : MonoBehaviour
    {
        private void Start()
        {
        LinkedLizt<int> listA = new LinkedLizt<int>();
        listA.insert(1, 0);
        Debug.Log("Size: " + listA.size().ToString());
        for (int i = 0; i<listA.size(); i++) {  Debug.Log(i.ToString() + ": " + listA.operate(i).data.ToString());}
        listA.insert(2, 1);
        Debug.Log("Size: " + listA.size().ToString());
        for (int i = 0; i < listA.size(); i++) { Debug.Log(i.ToString() + ": " + listA.operate(i).data.ToString()); }
        listA.insert(5, 0);
        Debug.Log("Size: " + listA.size().ToString());
        for (int i = 0; i < listA.size(); i++) { Debug.Log(i.ToString() + ": " + listA.operate(i).data.ToString()); }
        listA.erase(0);
        Debug.Log("Size: " + listA.size().ToString());
        for (int i = 0; i < listA.size(); i++) { Debug.Log(i.ToString() + ": " + listA.operate(i).data.ToString()); }
        listA.clear();
        listA.insert(15, 0);
        Debug.Log("Size: " + listA.size().ToString()); 
        for (int i = 0; i < listA.size(); i++) { Debug.Log(i.ToString() + ": " + listA.operate(i).data.ToString()); }
    }
    }
