// SequentialSearch.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#pragma once
#include <iostream>
#include <string>
#include "BiTree.h"
using namespace std;
/*
bool SequentialSearch(int* aArray, int aSize, int aValue) //Search an array one element at a time for a value, then return true if the value is found.
{
    for (int i = 0; i < aSize; i++)
    {
        if (aArray[i] == aValue)
            return true;
    }
    return false;
}

void InsertionSort(int* aArray, int aSize) //For each value in the array, move it backwards until the element before it is no longer greater than it.
{
    for (int i = 1; i < aSize; i++)
    {
        int counter = i;
        while (counter > 0 && aArray[counter-1] > aArray[counter])
        {
            swap(aArray[counter], aArray[counter - 1]);
            counter = counter - 1;
        }
    }
}

void MergeSort(int* a, int size) //Split an array in half until it is into one-unit pieces, then merge the pieces back together in sorted order
{
    int start = *a;
    int* endpoint = a + ((size - 1) * (sizeof(a[0])) / 4);
    int end = a[size - 1];
    if (start == end)
    {
        //cout << "Value: " + to_string(start) + " Size: " + to_string(size) + "\n";
        return;
    }
    int* midpoint = a + ((((size / 2))) * (sizeof(a[0])) / 4);
    int* midpoint2 = a + ((((size / 2) + size % 2)) * (sizeof(a[0])) / 4);
    int mid = *midpoint;
    int arraySize1 = (size / 2) + (size % 2);
    int arraySize2 = size / 2;
    MergeSort(a, arraySize1);
    MergeSort(midpoint2, arraySize2);
    for (int i = 1; i < size; i++)
    {
        int counter = i;
        while (counter > 0 && a[counter - 1] > a[counter])
        {
            swap(a[counter], a[counter - 1]);
            counter = counter - 1;
        }
    }

    //cout << "Start: "+ to_string(start)+ " Mid: "+ to_string(mid)+ " End: "+ to_string(end)+" Size: "+ to_string(size) + " Split into: " + to_string(arraySize1) + "&" + to_string(arraySize2) + "\n";
}

int BinarySearch(int* a, int size, int value) //Sort an array and search it for a value by dividing it in half until one of the pieces has a midpoint that is the intended value
{
    int* b = a;
    MergeSort(b, size);
    int end = size - 1;
    int start = 0;
    while (start != end)
    {
        //cout << to_string(start) + "-" + to_string(end) + "\n";
        int mid = (start + end) / 2;
        if (value == b[mid])
            return true;
        else if (value > b[mid])
            start = mid + 1;
        else
            end = mid;
    }
    return false;
}

int Partition(int* a, int start, int end)
{
    int pivot = a[end];
    int pivotSpot = end;
    for (int i = start + 1; i <= pivotSpot;) {
        if (a[i] <= pivot)
            i++;
        else
        {
            swap(a[pivotSpot - 1], a[i]);
            swap(a[pivotSpot], a[pivotSpot - 1]);
            pivotSpot--;
        }
    }
    return pivot;
}

void QuickSort(int* a, int start, int end)
{
    if (start < end) {
        int part = Partition(a, start, end);
        QuickSort(a, part+1, end);
        QuickSort(a, start, part-1);
    }
}
*/
int main()
{
    BinaryTree b;
    b.Insert(4);
    b.Insert(2);
    b.Insert(1);
    b.Insert(6);
    b.Insert(5);
    b.Insert(0);
    cout << to_string(b.Contains(4));
    cout << to_string(b.Contains(0));
    cout << to_string(b.Contains(5));
    cout << to_string(b.Contains(3));
    cout << endl;
    b.InOrderPrint();
    cin.get();
    //The test arrays: an empty array, a sorted array, an unsorted array with a duplicate value, and a reverse-sorted array
/*    int test1[1];
    int test2[10]{1,2,3,4,5, 6, 7, 8, 9, 10};
    int test3[10]{ 5,2, 1,0,2 , 3, 4, 7, 5, 1};
    int test4[5]{ 5,4,3,2,1 };
    cout<<"\n";
    cout << "Testing each array for 9, then 5. Expected sequence is 00010101\n"; //Testing each array for 9, then 5. Expected sequence is 00010101
    cout << to_string(SequentialSearch(test1, sizeof(test1) / sizeof(test1[0]), 9));
    cout << to_string(SequentialSearch(test1, sizeof(test1) / sizeof(test1[0]), 5));
    cout << to_string(SequentialSearch(test2, sizeof(test2) / sizeof(test2[0]), 9));
    cout << to_string(SequentialSearch(test2, sizeof(test2) / sizeof(test2[0]), 5));
    cout << to_string(SequentialSearch(test3, sizeof(test3) / sizeof(test3[0]), 9));
    cout << to_string(SequentialSearch(test3, sizeof(test3) / sizeof(test3[0]), 5));
    cout << to_string(SequentialSearch(test4, sizeof(test4) / sizeof(test4[0]), 9));
    cout << to_string(SequentialSearch(test4, sizeof(test4) / sizeof(test4[0]), 5)) + "\n";
    cout << "Sorting each array, then printing their values. Expected sequence is 12345,01225,12345\n"; //Sorting each array, then printing their values. Expected sequence is 12345,01225,12345
    InsertionSort(test1, sizeof(test1) / sizeof(test1[0]));
    for (int i = 0; i < sizeof(test1) / sizeof(test1[0]); i++)
    {
        if(test1[i]!= -858993460)
        cout << to_string(test1[i]);
    } cout << "\n";
    InsertionSort(test2, sizeof(test2) / sizeof(test2[0]));
    for (int i = 0; i < sizeof(test2) / sizeof(test2[0]); i++)
    {
        if (test2[i] != -858993460)
            cout << to_string(test2[i]);
    } cout << "\n";
    InsertionSort(test3, sizeof(test3) / sizeof(test3[0]));
    for (int i = 0; i < sizeof(test3) / sizeof(test3[0]); i++)
    {
        if (test3[i] != -858993460)
            cout << to_string(test3[i]);
    } cout << "\n";
    InsertionSort(test4, sizeof(test4) / sizeof(test4[0]));
    for (int i = 0; i < sizeof(test4) / sizeof(test4[0]); i++)
    {
        if (test4[i] != -858993460)
            cout << to_string(test4[i]);
    } cout << "\n";
    cin.get();
    MergeSort(test3, sizeof(test3) / sizeof(test3[0]));
    for (int i = 0; i < sizeof(test3) / sizeof(test3[0]); i++)
    {
        if (test3[i] != -858993460)
            cout << to_string(test3[i]);
    } cout << "\n";
    cin.get();
    */
/*    int test1[1];
    int test2[10]{ 1,2,3,4,5, 6, 7, 8, 9, 10 };
    int test3[10]{ 5,2, 1,0,2 , 3, 4, 7, 5, 1 };
    int test4[5]{ 5,4,3,2,1 };
    cout << "Testing each array for 11, then 4. Expected sequence is 00010101\n"; //Testing each array for 11, then 4. Expected sequence is 00010101
    cout << to_string(BinarySearch(test1, sizeof(test1) / sizeof(test1[0]), 11));
    cout << to_string(BinarySearch(test1, sizeof(test1) / sizeof(test1[0]), 4));
    cout << to_string(BinarySearch(test2, sizeof(test2) / sizeof(test2[0]), 11));
    cout << to_string(BinarySearch(test2, sizeof(test2) / sizeof(test2[0]), 4));
    cout << to_string(BinarySearch(test3, sizeof(test3) / sizeof(test3[0]), 11));
    cout << to_string(BinarySearch(test3, sizeof(test3) / sizeof(test3[0]), 4));
    cout << to_string(BinarySearch(test4, sizeof(test4) / sizeof(test4[0]), 11));
    cout << to_string(BinarySearch(test4, sizeof(test4) / sizeof(test4[0]), 4)) + "\n";
    cin.get();
    cout << "Sorting each array, then printing their values. Expected sequence is 12345678910,0112234557,12345\n"; //Sorting each array, then printing their values. Expected sequence is 12345,01225,12345
    MergeSort(test1, sizeof(test1) / sizeof(test1[0]));
    for (int i = 0; i < sizeof(test1) / sizeof(test1[0]); i++)
    {
        if (test1[i] != -858993460)
            cout << to_string(test1[i]);
    } cout << "\n";
    MergeSort(test2, sizeof(test2) / sizeof(test2[0]));
    for (int i = 0; i < sizeof(test2) / sizeof(test2[0]); i++)
    {
        if (test2[i] != -858993460)
            cout << to_string(test2[i]);
    } cout << "\n";
    MergeSort(test3, sizeof(test3) / sizeof(test3[0]));
    for (int i = 0; i < sizeof(test3) / sizeof(test3[0]); i++)
    {
        if (test3[i] != -858993460)
            cout << to_string(test3[i]);
    } cout << "\n";
    MergeSort(test4, sizeof(test4) / sizeof(test4[0]));
    for (int i = 0; i < sizeof(test4) / sizeof(test4[0]); i++)
    {
        if (test4[i] != -858993460)
            cout << to_string(test4[i]);
    } cout << "\n";
    cin.get();
    cout << "\n";
    */
/*   int test1[1];
    int test2[10]{ 1,2,3,4,5, 6, 7, 8, 9, 10 };
    int test3[10]{ 5,2, 1,0,2 , 3, 4, 7, 5, 1 };
    int test4[5]{ 5,4,3,2,1 };
    int test5[3]{ 1,2,3 };
    cout << "Sorting each array, then printing their values. Expected sequence is 12345678910,0112234557,12345\n"; //Sorting each array, then printing their values. Expected sequence is 12345,01225,12345
    //QuickSort(test1, 0, sizeof(test1) / sizeof(test1[0]));
    for (int i = 0; i < sizeof(test1) / sizeof(test1[0]); i++)
    {
        if (test1[i] != -858993460)
            cout << to_string(test1[i]);
    } cout << "\n";
    cin.get();
    //QuickSort(test2, 0, (sizeof(test2) / sizeof(test2[0]))-1);
    QuickSort(test5, 0, (sizeof(test5) / sizeof(test5[0])) - 1);
    for (int i = 0; i < sizeof(test2) / sizeof(test2[0]); i++)
    {
        if (test2[i] != -858993460)
            cout << to_string(test2[i]);
    } cout << "\n";
    cin.get();
    //QuickSort(test3, 0, (sizeof(test3) / sizeof(test3[0])) - 1);
    for (int i = 0; i < sizeof(test3) / sizeof(test3[0]); i++)
    {
        if (test3[i] != -858993460)
            cout << to_string(test3[i]);
    } cout << "\n";
    //QuickSort(test4, 0, (sizeof(test4) / sizeof(test4[0])) - 1);
    for (int i = 0; i < sizeof(test4) / sizeof(test4[0]); i++)
    {
        if (test4[i] != -858993460)
            cout << to_string(test4[i]);
    } cout << "\n";
    cin.get();
    cout << "\n";
    */

}
