#include <iostream>

class BinaryTree
{
public:
    BinaryTree() {
        mRoot = nullptr;
    };

    // Remember to properly delete any allocated memory
    ~BinaryTree() { delete mRoot; }

    // Insert a new node into the tree
    void Insert(int aValue) {
        BTreeNode* newNode = new BTreeNode();
        newNode->value = aValue;
        if (mRoot == nullptr)
        {
            mRoot = newNode;
        }
        else
        {
            BTreeNode* currentNode = mRoot;
            BTreeNode* parent;
            while (true)
            {
                parent = currentNode;
                if (aValue < currentNode->value)
                {
                    currentNode = currentNode->left;
                    if (currentNode == nullptr)
                    {
                        parent->left = newNode;
                        return;
                    }
                }
                else
                {
                    currentNode = currentNode->right;
                    if (currentNode == nullptr)
                    {
                        parent->right = newNode;
                        return;
                    }
                }
            }
        }
    }

    // Check if the tree contains the given value
    bool Contains(int aValue) 
    {
        BTreeNode* currentNode = mRoot;
        while (true)
        {
            if (currentNode == nullptr) return false;
            if (aValue == currentNode->value)
            {
                return true;
            }
            else if (aValue < currentNode->value)
            {
                currentNode = currentNode->left;
            }
            else if (aValue > currentNode->value)
            {
                currentNode = currentNode->right;
            }
        }
    }

    // Print all of the elements in the Binary Tree, in order
    void InOrderPrint() {
        inOrderPrint(mRoot);
    }

private:
    struct BTreeNode
    {
        BTreeNode* left = nullptr;
        BTreeNode* right = nullptr;
        int value;
    };
    void inOrderPrint(BTreeNode* currentNode) 
    {
        if (currentNode != nullptr) {
            inOrderPrint(currentNode->left);
            std::cout << currentNode->value << " ";
            inOrderPrint(currentNode->right);
            
        }
    }
    BTreeNode* mRoot;
};