// SpeedRunAnyPercent.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int readNext(string input)
{
    bool numberDone;
    string nextStr;
    int nextNum;
    for (int i = 0; i < input.length(); i++)
    {
        if (input[i] == ' ' || input[i] == NULL)
        {
            break;
        }
        else
        {
            nextStr += input[i];
        }
    }
    nextNum = stoi(nextStr);
    return nextNum;
}

string deleteFirst(string input)
{
    bool first = true;
    string nextStr = "";
    for (int i = 0; i < input.length(); i++)
    {
        if (!first)
        {
            nextStr += input[i];
        }
        else if(input[i]==' ')
        {
            first = false;
        }
    }
    return nextStr;
}

int main()
{
    int test[1000];
    string line;
    ifstream file;
    int elements = 1000;
    string data = "";
    string testdata = "";
    int target = 0;
    file.open("Numbers.txt");
    if (file.is_open())
    {
        int lineCount = 0;
        
        while (getline(file, line)) 
        {
            if (lineCount == 0)
            {
                elements = readNext(line);
                cout << elements;
            }
            else if (lineCount == 1)
            {
                data = line;
                for (int i = 0; i < elements; i++)
                {
                    test[i] = readNext(line);
                    cout << readNext(line) << " ";
                    line = deleteFirst(line);
                }
            }
            else if (lineCount == 2)
            {
                target = readNext(line);
                cout << target;
            }
            else break;
            cout <<'\n';
            lineCount++;
        }
        file.close();
    }
    testdata = data;
    bool testing = true;
    for (int i = 0; i < elements; i++)
    {
        if (target == readNext(testdata))
        {
            testing = false;
            cout << "True" << endl;
            break;
        }
        testdata = deleteFirst(testdata);
    }
    if (testing) cout << "False" << endl;
    testing = true;
    for (int i = 0; i < elements; i++)
    {
        if (testing)
        {
            target = test[i];
            for (int j = 0; j < elements; j++)
            {
                if (test[j] == target && j != i)
                {
                    testing = false;
                    cout << "True" << endl;
                    break;
                }
            }
        }
    }
    if (testing) cout << "False" << endl;
    cin.get();
}